# IDO Reports Module for Icinga Web 2 | (c) 2018-2019 Icinga Development Team <info@icinga.com> | GPLv2+

%global revision 1
%global module_name idoreports

%global icingaweb_min_version 2.6.0

Name:           icingaweb2-module-%{module_name}
Version:        0.9.1
Release:        %{revision}%{?dist}
Summary:        IDO Reports - Icinga Web 2 module
Group:          Applications/System
License:        GPLv2+
URL:            https://icinga.com
Source0:        https://github.com/Icinga/icingaweb2-module-%{module_name}/archive/v%{version}.tar.gz
#/icingaweb2-module-%{module_name}-%{version}.tar.gz
BuildArch:      noarch

%global basedir %{_datadir}/icingaweb2/modules/%{module_name}

Requires:       icingaweb2 >= %{icingaweb_min_version}
Requires:       php-Icinga >= %{icingaweb_min_version}

Requires:       icingaweb2-module-reporting >= 0.9

%description
Icinga IDO Reports provides host and service availability reports for Icinga
1.x and Icinga 2.x based on the monitoring database (IDO).

%prep
%setup -q
#-n icingaweb2-module-%{module_name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{basedir}

cp -r * %{buildroot}%{basedir}

%clean
rm -rf %{buildroot}

%preun
set -e

# Only for removal
if [ $1 == 0 ]; then
    echo "Disabling icingaweb2 module '%{module_name}'"
    rm -f /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%files
%doc README.md LICENSE

%defattr(-,root,root)
%{basedir}

%changelog
* Fri Aug 30 2019 Markus Frosch <markus.frosch@icinga.com> - 0.9.1-1
- Initial package version
